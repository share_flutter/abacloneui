import 'package:aba_clone_ui/model/posts_response_entity.dart';
import 'package:aba_clone_ui/ui/widget/menu_item.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  var dio = Dio();
  Response response;
  ResponseBody responseBody;
  bool isLoading = false;

  List<PostsResponseEntity> postsResponseEntity = [];
  String title;
  PostsResponseEntity post = PostsResponseEntity();
  Future getPostData() async {
    setState(() {
      isLoading = false;
    });
    response = await dio.get("https://jsonplaceholder.typicode.com/posts");
    print("data=> ${response.statusCode}");
    if (response.statusCode == 200) {
      for (var value in response.data) {
        post.title = value['title'];
        setState(() {
          isLoading = true;
          postsResponseEntity.add(post);
        });
      }
    } else {
      print("erro");
    }
    return postsResponseEntity;
    print("data list=> ${postsResponseEntity.length}");
  }

  @override
  void initState() {
    super.initState();
    getPostData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF0A557C),
          centerTitle: true,
          leading: Icon(Icons.menu),
          title: Text(
            "ABA Clone UI",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          actions: [
            InkWell(
              onTap: () {
                print("Hello world Mr.sophea");
              },
              child: Container(
                child: Icon(Icons.notifications_none),
                padding: const EdgeInsets.only(right: 20),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20),
              child: Icon(Icons.wifi_calling),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 400,
                  decoration: BoxDecoration(
                      gradient: RadialGradient(
                          colors: [Colors.white, Color(0xFF0A557C)])),
                  child: GridView.count(
                    crossAxisSpacing: 1,
                    mainAxisSpacing: 1,
                    crossAxisCount: 3,
                    children: [
                      MenuItem(
                        icon: Icons.account_balance_wallet,
                        tile: "Account",
                        titleColor: Colors.white,
                        index: 0,
                      ),
                      MenuItem(
                        icon: Icons.credit_card,
                        tile: "Card",
                        titleColor: Colors.white,
                        index: 1,
                      ),
                      MenuItem(
                        icon: Icons.monetization_on,
                        tile: "Payment",
                        titleColor: Colors.white,
                      ),
                      MenuItem(
                        icon: Icons.event_note,
                        tile: "New Account",
                        titleColor: Colors.white,
                      ),
                      MenuItem(
                        icon: Icons.atm,
                        tile: "Cash to ATM",
                        titleColor: Colors.white,
                      ),
                      MenuItem(
                        icon: Icons.transfer_within_a_station,
                        tile: "Transfer",
                        titleColor: Colors.white,
                      ),
                      MenuItem(
                        icon: Icons.qr_code,
                        tile: "Scan QR",
                        titleColor: Colors.white,
                      ),
                      MenuItem(
                        icon: Icons.monetization_on_sharp,
                        tile: "Loan",
                        titleColor: Colors.white,
                      ),
                      MenuItem(
                        icon: Icons.location_on,
                        tile: "Locator",
                        titleColor: Colors.white,
                      ),
                    ],
                  ),
                ),
                Flexible(
                  child: Container(
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: postsResponseEntity?.length ?? 0,
                        itemBuilder: (context, index) {
                          return Material(
                            color: index % 2 == 0
                                ? Colors.lightBlueAccent
                                : Colors.red,
                            child: Container(
                                height: 100,
                                padding: EdgeInsets.only(left: 10),
                                width: MediaQuery.of(context).size.width,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${index % 2 == 0 ? "Quick Transfer" : "Quick Payment"}",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18),
                                    ),
                                    Text(
                                      "${postsResponseEntity[index]?.title ?? ''}",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 14),
                                    )
                                  ],
                                )),
                          );
                        }),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}

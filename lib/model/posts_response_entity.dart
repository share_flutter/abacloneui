import 'package:aba_clone_ui/generated/json/base/json_convert_content.dart';

class PostsResponseEntity with JsonConvert<PostsResponseEntity> {
  double userId;
  double id;
  String title;
  String body;
}

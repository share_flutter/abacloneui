import 'package:aba_clone_ui/ui/view/account_view.dart';
import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  final String tile;
  final IconData icon;
  final Color titleColor;
  final int index;
  MenuItem({this.tile, this.icon, this.titleColor, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF0A557C),
      child: MaterialButton(
        onPressed: () {
          if (index == 0) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AccountView()));
          } else {
            print("error");
          }
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              size: 40,
              color: titleColor,
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "$tile",
              style: TextStyle(color: titleColor, fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}
